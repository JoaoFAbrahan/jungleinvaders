using CHARACTERS;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CODE_PlayerClass : MonoBehaviour
{
    public int maxHealth = 1000;
    public int points;
    public LayerMask enemyLayer;

    private int _health;
    private bool _bIsDeath;
    private Collider2D _collider2D;


    private void Start()
    {
        _health = maxHealth;
    }

    private void Update()
    {
        //Debug.Log(_health);
        CheckState();
        CheckCollision();
        DeathCondition();
    }


    private void CheckCollision()
    {
        if (ColliderState())
        {
            if (_collider2D.GetComponent<CODE_CharacterHumans>() != null)
            {
                TakeDamage(_collider2D.gameObject, true);
            }
            else
            {
                TakeDamage(_collider2D.gameObject, false);
            }
        }
    }

    private bool ColliderState()
    {
        _collider2D = Physics2D.OverlapBox(transform.position, new Vector2(13f, 0.3f), 0f, enemyLayer); ;
        return _collider2D != null;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(transform.position, new Vector2(13f, 0.3f));
    }

    private void TakeDamage(GameObject ObjectRef, bool Controller)
    {
        if (Controller)
        {
            CODE_CharacterHumans ScriptRef = ObjectRef.GetComponent<CODE_CharacterHumans>();

            switch (ScriptRef.enemyType)
            {
                case ENUM_EnemyType.Operario:
                    _health -= 20;
                    break;
                case ENUM_EnemyType.Lenhador:
                    _health -= 40;
                    break;
                case ENUM_EnemyType.Minerador:
                    _health -= 60;
                    break;
            }
        }
        else
        {

        }

        Destroy(ObjectRef.gameObject);
    }

    private void CheckState()
    {
        if (_health <= 0)
        {
            _bIsDeath = true;
        }
        else
        {
            _bIsDeath = false;
        }
    }

    private void DeathCondition()
    {
        if (_bIsDeath)
        {
            // GAME OVER
            Debug.Log("Game Over!!!");
        }
    }
}
