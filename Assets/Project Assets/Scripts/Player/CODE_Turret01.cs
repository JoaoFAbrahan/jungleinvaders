using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class CODE_Turret01 : MonoBehaviour
{
    public Transform target;
    public Animator animator;

    [Header("Attributes")]
    public float range = 2f;
    public float fireRate = 1f;
    private float fireCountdown = 0f;
    public float damage = 10f;
    public float health = 50f;


    [Header("Unity Setup")]
    public string enemyTag = "Enemy";

    public GameObject bulletPrefab;
    public Transform firePoint;

    
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("updateTarget", 0f, 0.5f);
    }

    void updateTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;

        foreach (GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }

        if (nearestEnemy != null && shortestDistance <= range)
        {
            target = nearestEnemy.transform;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (fireCountdown >= 0f)
        {
            animator.SetBool("isAttacking", false);
        }
        if (target == null)
        {
            return;
        }

        if (fireCountdown <= 0f)
        {
            Shoot();
        animator.SetBool("isAttacking", true);
            fireCountdown = 1f/fireRate;
        }

        fireCountdown -= Time.deltaTime;
    }

    void Shoot()
    {
        GameObject bulletGameObject = (GameObject)Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        CODE_Bullet bullet = bulletGameObject.GetComponent<CODE_Bullet>();

        if (bullet != null)
        {
            bullet.Seek(target);
        }
    }
 
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}
