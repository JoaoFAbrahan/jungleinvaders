using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CHARACTERS
{

}
public class CODE_AttackClass : MonoBehaviour
{
    public float attackTimeDelay;

    private bool _attackCoroutineState;
    private bool _attackDelay;

    
    private void Start()
    {
        this._attackCoroutineState = false;
    }


    private IEnumerator AttackDelay(float DelayTime)
    {
        _attackCoroutineState = false;
        yield return new WaitForSeconds(DelayTime);
        this._attackDelay = false;
        this._attackCoroutineState = true;
    }

    public virtual void AttackTurret(float DelayTime, GameObject TurretRef)
    {
        if (this._attackDelay)
        {
            if (_attackCoroutineState)
            {
                StartCoroutine(AttackDelay(DelayTime));
            }
        }
        else
        {
            // Attack
        }
    }
}
