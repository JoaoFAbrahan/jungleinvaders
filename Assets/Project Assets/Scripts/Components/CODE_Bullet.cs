using CHARACTERS;
using System.Security.Cryptography;
using UnityEngine;

public class CODE_Bullet : MonoBehaviour
{
    public float speed = 30f;
    public int Damage = 10;

    private Transform _target;


    // Update is called once per frame
    void Update()
    {
        if (_target == null)
        {
            Destroy(gameObject);
            return; 
        }

        Vector3 dir = _target.position - transform.position;
        float distanceThisFrame = speed * Time.deltaTime;

        if(dir .magnitude <= distanceThisFrame)
        {
            HitTarget();
            return;
        }

        transform.Translate(dir.normalized * distanceThisFrame, Space.World);
    }


    public void Seek(Transform _target)
    {
        this._target = _target;
    }
    
    void HitTarget()
    {            
        Destroy(gameObject);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        collision.transform.parent.gameObject.GetComponent<CODE_CharacterHumans>().DamageReceived(Damage);
    }
}
