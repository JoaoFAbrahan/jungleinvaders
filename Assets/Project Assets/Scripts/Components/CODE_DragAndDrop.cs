using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CODE_DragAndDrop : MonoBehaviour
{
    public float dragSpeed = 100;
    public bool _bSpawnConfirm;

    private Camera _camera;
    private Transform _transformToDrag = null;
    private Vector3 _dragOffset = Vector3.zero;

    private void Start()
    {
        _bSpawnConfirm = false;
        _camera = Camera.main;
    }

    private void Update()
    {
        // Movimenta o objeto ate o usuario clicar novamente
        if(!_bSpawnConfirm)
        {
            Vector3 origin = GetInputInWorldSpace();

            RaycastHit2D hitUp = Physics2D.Raycast(origin, Vector2.up);
            RaycastHit2D hitDown = Physics2D.Raycast(origin, Vector2.down);

            if (hitUp.collider != null && hitDown.collider != null && hitDown.collider == hitUp.collider)
            {
                Debug.Log("cliked: " + hitUp.collider.gameObject.name);

                if (hitUp.collider.tag == "Turret")
                {
                    _dragOffset = hitUp.collider.transform.position - origin;
                    _transformToDrag = hitUp.collider.transform;
                }
            }
        }        

        if (Input.GetMouseButtonUp(0) || Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Ended)
        {
            _transformToDrag = null;            
        }

        if (_transformToDrag != null)
        {
            _transformToDrag.position = Vector3.MoveTowards(_transformToDrag.position, GetInputInWorldSpace() + _dragOffset / 2, dragSpeed + Time.deltaTime);
            _bSpawnConfirm = true;
        }
    }


    private Vector3 GetInputInWorldSpace()
    {
        Vector2 origin;

        if (Input.touches.Length == 0)
        {
            origin = _camera.ScreenToWorldPoint(Input.mousePosition);
        }
        else
        {
            origin = _camera.ScreenToWorldPoint(Input.touches[0].position);
        }

        return new Vector3(origin.x, origin.y, 10);
    }
}
