using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CHARACTERS
{
	public enum ENUM_EnemyType
	{
		Operario,
		Lenhador,
		Minerador,
		Trator,
		Caminhao
	}
}
