using MAP_COMPONENTS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CODE_ClimateClass : MonoBehaviour
{
    public ENUM_ClimateType climateState;

    private CODE_SpawnClass _spawnClassRef;
    private bool _bFirstCliamateChange;
    private bool _bSecondClimateChange;

    private void Start()
    {
        // Inicializando variáveis
        climateState = RandomClimate();
        _bFirstCliamateChange = false;
        _bSecondClimateChange = false;
        _spawnClassRef = gameObject.GetComponent<CODE_SpawnClass>();
    }

    private void Update()
    {
        if (_spawnClassRef.firstWave_qtd <= 0 && _bFirstCliamateChange == false)
        {
            _bFirstCliamateChange = true;
            climateState = RandomClimate();
        }

        if (_spawnClassRef.secondWave_qtd <= 0 && _bSecondClimateChange == false)
        {
            _bSecondClimateChange= true;
            climateState = RandomClimate();
        }
    }


    private ENUM_ClimateType RandomClimate()
    {
        //Debug.Log("Change climate");
        int SeedRand;
        SeedRand = Random.Range(0, 2);

        switch (SeedRand)
        {
            case 0:
                return ENUM_ClimateType.Calor;
            case 1:
                return ENUM_ClimateType.Ventania;
            default:
                return ENUM_ClimateType.Chuvoso;
        }
    }
}
