using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CODE_ColliderClass : MonoBehaviour
{
    public LayerMask enemyLayer;
    private Collider2D _collider2D;


    public bool ColliderState()
    {
        _collider2D = Physics2D.OverlapCircle(transform.position, 0.3f, enemyLayer);
        return _collider2D != null;
    }
    
    public bool DetectTurret()
    {
        if (_collider2D != null)
        {
            if (_collider2D.tag == "Turret")
                return true;
            else
                return false;
        }

        return false;
    }

    public GameObject GetEnemyReference()
    {
        return _collider2D.gameObject;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, 0.3f);
    }
}
