using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CHARACTERS
{
    public class CODE_AnimatorClass : MonoBehaviour
    {
        private Animator _animator;


        public void SetAnimator(GameObject GameObjRef)
        {
            _animator = GameObjRef.GetComponent<Animator>();
        }

        public void PlayAnimation(string AnimationName)
        {
            _animator.SetTrigger(AnimationName);
        }

        public void PlayAnimation(string AnimationName, bool Value)
        {
            _animator.SetBool(AnimationName, Value);
        }
    }
}