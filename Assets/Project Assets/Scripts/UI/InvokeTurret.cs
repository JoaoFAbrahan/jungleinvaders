using MAP_COMPONENTS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvokeTurret : MonoBehaviour
{
    [SerializeField] public GameObject currentTurret;

    private Camera _camera;
    private CODE_DragAndDrop GameManeger;

    // Start is called before the first frame update
    void Start()
    {
        GameManeger = FindObjectOfType<CODE_DragAndDrop>();
        _camera = Camera.main;
    }

    public void SpawnTurret()
    {
        Instantiate(currentTurret, _camera.ScreenToWorldPoint(Input.mousePosition), Quaternion.identity);
        GameManeger._bSpawnConfirm = false;
    }
}
