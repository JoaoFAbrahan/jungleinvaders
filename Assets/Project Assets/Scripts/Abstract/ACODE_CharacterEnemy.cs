using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

namespace CHARACTERS
{
    public abstract class ACODE_CharacterEnemy : MonoBehaviour
    {
        public float moveSpeed = 1f;
        public int health = 75;
        public int damage = 20;
        public float moveTimeDelay = 0.2f;
        public float attackTimeDelay = 1f;
        public ENUM_EnemyType enemyType;
        public LayerMask enemyLayer;

        protected bool _bIsDeath;
        protected bool _bIsMoving;
        protected bool _bIsAttack;
        protected bool _moveCoroutineState;
        protected bool _attackCoroutineState;
        protected bool _attackDelay;
        protected Vector3 _target;
        protected ENUM_ClimateType _currentClimate;
        protected CODE_AttackClass _attackClass;
        protected CODE_ColliderClass _colliderBase;
        protected CODE_ColliderClass _colliderLeft;
        protected CODE_ColliderClass _colliderRight;
        protected CODE_AnimatorClass _animatorClass;


        /// <summary>
        /// Passa a dire��o do movimento
        /// </summary>
        /// <returns></returns>
        protected Vector3 SelectDirection()
        {
            bool Direction = Randomize();

            // Movimenta o inimigo ----> true = direita, false = esquerda
            if (Direction)
            {
                // Verifica os limites do mapa
                if (CheckMapLimit(Direction))
                {
                    // Limite da Direita (Tende a mover para a esquerda)
                    if (this._colliderLeft.ColliderState()) // Verifica disponibilidade a esquerda
                    {
                        // Verifica se a colisao na esquerda eh uma turret
                        if (this._colliderLeft.DetectTurret())
                        {
                            this._bIsAttack = true;
                        }

                        // Colis�o em ambos os lados (Inimigo fica parado)
                        return new Vector3(0f, 0f, 0f);
                    }
                    else
                    {
                        // Move para a Esquerda
                        return new Vector3(-0.5f, -0.755f, 0f);
                    }
                }
                else
                {
                    
                    // Tende a mover para a Direita
                    if (this._colliderRight.ColliderState()) // Verifica disponibilidade a direita
                    {
                        // Verifica se a colisao na direita eh uma turret
                        if (this._colliderRight.DetectTurret())
                        {
                            this._bIsAttack = true;
                        }

                        // Tenta mover para a Esquerda                        
                        if (this._colliderLeft.ColliderState()) // Verifica disponibilidade a Esquerda
                        {
                            // Verifica se a colisao na esquerda eh uma turret
                            if (this._colliderLeft.DetectTurret())
                            {
                                this._bIsAttack = true;
                            }

                            // Colis�o em ambos os lados (Inimigo fica parado)
                            return new Vector3(0f, 0f, 0f);
                        }
                        else
                        {
                            // Move para a Esquerda
                            return new Vector3(-0.5f, -0.755f, 0f);
                        }

                    }
                    else
                    {
                        // Move pra a Direita
                        return new Vector3(0.5f, -0.755f, 0f);
                    }
                }
            }
            else
            {
                // Verifica os limites do mapa
                if (CheckMapLimit(Direction))
                {
                    // Limite da esquerda (Tende a mover para a direita)
                    if (this._colliderRight.ColliderState()) // Verifica disponibilidade a direita
                    {
                        // Verifica se a colisao na direita eh uma turret
                        if (this._colliderRight.DetectTurret())
                        {
                            this._bIsAttack = true;
                        }

                        // Colis�o em ambos os lados (Inimigo fica parado)
                        return new Vector3(0f, 0f, 0f);
                    }
                    else
                    {
                        // Move para a Direita
                        return new Vector3(0.5f, -0.755f, 0f);
                    }
                }
                else
                {
                    // Tende a mover para a Esquerda
                    if (this._colliderLeft.ColliderState()) // Verifica disponibilidade a Esquerda
                    {
                        // Verifica se a colisao na Esquerda eh uma turret
                        if (this._colliderLeft.DetectTurret())
                        {
                            this._bIsAttack = true;
                        }

                        // Tenta mover para a Direita                        
                        if (this._colliderRight.ColliderState()) // Verifica disponibilidade a Direita
                        {
                            // Verifica se a colisao na Direita eh uma turret
                            if (this._colliderRight.DetectTurret())
                            {
                                this._bIsAttack = true;
                            }

                            // Colis�o em ambos os lados (Inimigo fica parado)
                            return new Vector3(0f, 0f, 0f);
                        }
                        else
                        {
                            // Move para a Direita
                            return new Vector3(0.5f, -0.755f, 0f);
                        }

                    }
                    else
                    {
                        // Move pra a Esquerda
                        return new Vector3(-0.5f, -0.755f, 0f);
                    }
                }
            }
        }

        /// <summary>
        /// Pausa temporariamente entre os movimentos
        /// </summary>
        /// <param name="DelayTime"></param>
        /// <returns></returns>
        protected IEnumerator TranslateDelay(float DelayTime)
        {
            this._moveCoroutineState = false;
            yield return new WaitForSeconds(DelayTime);
            // Start anima��o de movimento
            _animatorClass.PlayAnimation("WalkTrigger");
            this._bIsMoving = true;
            this._moveCoroutineState = true;
        }

        /// <summary>
        /// Movimento de trnasla��o do inimigo e start do delay de movimento
        /// </summary>
        protected void Movimentation()
        {
            if (this._bIsMoving)
            {
                // Translada para a pr�xima posi��o
                Vector3 EnemyTranslate = this._target - this.transform.position;
                this.transform.Translate(EnemyTranslate.normalized * this.moveSpeed * Time.deltaTime, Space.World);


                // Ativa a condi��o de Delay quando a posi��o � alcan�ada
                if (Vector3.Distance(this.transform.position, this._target) <= 0.01f)
                {
                    _animatorClass.PlayAnimation("WalkTrigger");
                    this._bIsMoving = false;
                }
            }
            else
            {
                if (_moveCoroutineState)
                {
                    // Inicializa a coroutine
                    StartCoroutine(TranslateDelay(this.moveTimeDelay));
                    this._target = this.transform.position + SelectDirection();
                }
            }
        }

        /// <summary>
        /// Randomiza a dire��o de movimento
        /// </summary>
        /// <returns></returns>
        private bool Randomize()
        {
            return System.Convert.ToBoolean(Random.Range(0, 2));
        }

        /// <summary>
        /// Verifica o limite do mapa
        /// </summary>
        /// <param name="Direction"></param>
        /// <returns></returns>
        private bool CheckMapLimit(bool Direction)
        {
            // Verifica os limites do mapa (Esquerda ou Direita)
            if (Direction)
            {
                // Limite da Direita
                if ((this.transform.position.x + 0.5f) > 4.51f)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                // Limite da Esquerda
                if ((this.transform.position.x - 0.5f) < -7.51f)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}