using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;

namespace MAP_COMPONENTS
{
    public class CODE_SpawnClass : MonoBehaviour
    {
        // Variaveis para serializacao do dicionario
        [Serializable]
        public struct KeyValuePair
        {
            public GameObject Key;
            public int Value;
        }
        public List<KeyValuePair> enemyList = new List<KeyValuePair>();
        private Dictionary<GameObject, int> _spawnList = new Dictionary<GameObject, int>();

        public float timeWaves;
        public int firstWave_qtd;
        public int secondWave_qtd;

        
        private Transform _spawnPos;
        private GameObject _spawnPointRef;
        private CODE_EnemySpawnList _enemySpawnList;
        private bool _bSstopSpawning = false;


        private void Start()
        {
            // Pega a posi��o de Spawn
            _enemySpawnList = FindObjectOfType<CODE_EnemySpawnList>();
            _spawnPos = _enemySpawnList.GetSpawnPosition();

            // Pega a referencia da posicao de spawn randomizada
            _spawnPointRef = _enemySpawnList.GetSpawnPointReference();

            // Montagem do dicionario
            foreach (var KVP in enemyList)
            {
                _spawnList[KVP.Key] = KVP.Value;
            }

            // Starta o spawn
            InvokeRepeating("SpawnObject", 1f, timeWaves);
        }

        private void Update()
        {

        }


        private void SpawnObject()
        {
            if (!_spawnPointRef.transform.GetChild(0).GetComponent<CODE_ColliderClass>().ColliderState())
            {
                GameObject SpawnObject = null;

                // Verifica o round
                if (firstWave_qtd > 0)
                {
                    // Define o objeto a ser spawnado
                    SpawnObject = DictionaryEditElements();
                    firstWave_qtd--;
                }
                else if (secondWave_qtd > 0)
                {
                    // Define o objeto a ser spawnado
                    SpawnObject = DictionaryEditElements();
                    secondWave_qtd--;
                }
                else
                {
                    // Define o objeto a ser spawnado
                    if (_spawnList.Count > 0)
                    {
                        SpawnObject = DictionaryEditElements();
                    }
                    else
                    {
                        _bSstopSpawning = true;
                    }
                }
                if (SpawnObject != null)
                    Instantiate(SpawnObject, _spawnPos.position, _spawnPos.rotation);
                if (_bSstopSpawning)
                    CancelInvoke("SpawnObject");
            }
            else
            {
                Debug.Log("In collision");
            }
        }

        private GameObject DictionaryEditElements()
        {
            GameObject TempKey;
            int IndexNum = RandEnemy();
            TempKey = _spawnList.ElementAt(IndexNum).Key; // Pega a chave do elemento randomizado
            _spawnList[TempKey] = _spawnList.ElementAt(IndexNum).Value - 1; // Reduz a quantidade desse elemento na lista
            DictonaryCheckElementsQtd(IndexNum); // Verifica se ainda existe elementos desse tipo, caso nao exista ele remove da lista
            return TempKey;
        }

        private int RandEnemy()
        {
            return UnityEngine.Random.Range(0, _spawnList.Count);
        }

        private void DictonaryCheckElementsQtd(int Index)
        {
            if (_spawnList.ElementAt(Index).Value <= 0)
            {
                _spawnList.Remove(_spawnList.ElementAt(Index).Key);
            }
        }
    }
}