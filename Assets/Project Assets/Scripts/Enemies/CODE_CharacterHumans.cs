using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CHARACTERS
{
    public class CODE_CharacterHumans : ACODE_CharacterEnemy
    {
        public bool bClimateBuffDebuff;
        public ENUM_ClimateType buffClimate;
        public ENUM_ClimateType debuffClimate;

        private CODE_ClimateClass _climateClassRef;


        private void Start()
        {
            // Inicializando variaveis
            _bIsMoving = false;
            _bIsAttack = false;
            _moveCoroutineState = true;
            _bIsDeath = false;
            enemyType = ENUM_EnemyType.Operario;
            gameObject.AddComponent<CODE_AttackClass>();
            _attackClass = GetComponent<CODE_AttackClass>();
            _climateClassRef = FindObjectOfType<CODE_ClimateClass>();


            // inicializando as referencias dos colisores
            _colliderBase = this.transform.GetChild(1).GetComponent<CODE_ColliderClass>();
            _colliderLeft = this.transform.GetChild(2).GetComponent<CODE_ColliderClass>();
            _colliderRight = this.transform.GetChild(3).GetComponent<CODE_ColliderClass>();

            // inicializando a classe de animacao
            gameObject.AddComponent<CODE_AnimatorClass>();
            _animatorClass = GetComponent<CODE_AnimatorClass>();
            _animatorClass = transform.GetChild(0).GetComponent<CODE_AnimatorClass>();
            _animatorClass.SetAnimator(transform.GetChild(0).gameObject);
        }

        private void Update()
        {
            CheckState();
            CheckClimateState();
            //Debug.Log(_currentClimate);
        }


        public void DamageReceived(int Damage)
        {
            // Aplica dano no HP da entidade
            health -= Damage;            
        }

        private void DeathCondition()
        {
            // Destroi a entidade
            Destroy(this);
        }

        private void CheckState()
        {
            // Aplica morte caso o HP chegue a zero
            if (health <= 0)
            {
                _bIsDeath = true;
            }

            // Verifica o estado da entidade
            if (_bIsDeath)
            {
                DeathCondition();
            }
            else
            {
                if (_bIsAttack)
                {
                    AttackTurret();                    
                }
                else
                {
                    Movimentation();                    
                }
            }
        }

        private void AttackTurret()
        {
            bool bAttackCount = true;

            if (_colliderLeft.gameObject.tag == "Turret")
            {
                _attackClass.AttackTurret(attackTimeDelay, _colliderLeft.GetEnemyReference());
                bAttackCount = false;
            }
            else if (bAttackCount == true)
            {
                if (_colliderRight.gameObject.tag == "Turret")
                {
                    _attackClass.AttackTurret(attackTimeDelay, _colliderRight.GetEnemyReference());
                    bAttackCount = false;
                }
            }

            _bIsAttack = false;
        }  
        
        private void CheckClimateState()
        {
            if (bClimateBuffDebuff)
            {
                if (buffClimate == _currentClimate)
                {
                    // Aplica BUFF
                }
                else if (debuffClimate == _currentClimate)
                {
                    // Aplica DEBUFF
                }
            }
        }
    }
}