using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MAP_COMPONENTS
{
    public class CODE_EnemySpawnList : MonoBehaviour
    {
        private static Transform[] _spawnPoints;
        private int _spawnIndex;


        private void Awake()
        {
            //  Gera um array contendo todos os _spawnPoints
            _spawnPoints = new Transform[transform.childCount];

            for (int i = 0; i < _spawnPoints.Length; i++)
            {
                _spawnPoints[i] = transform.GetChild(i);
            }
            _spawnIndex = RandomizeSpawnPosition();
        }


        /// <summary>
        /// Randomiza uma index do array
        /// </summary>
        /// <returns></returns>
        public Transform GetSpawnPosition()
        {
            return _spawnPoints[_spawnIndex];
        }

        public GameObject GetSpawnPointReference()
        {
            return _spawnPoints[_spawnIndex].gameObject;
        }

        /// <summary>
        /// Retorna a Transform Position do ponto de spawn escolhido
        /// </summary>
        /// <returns></returns>
        private int RandomizeSpawnPosition()
        {
            // Randomiza um numero para usar como index de array de _spawnPoints
            int SeedRand;
            SeedRand = Random.Range(0, _spawnPoints.Length);

            return SeedRand;
        }
    }
}